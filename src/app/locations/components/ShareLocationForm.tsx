"use client";

import Button from "@/components/Ui/Button";
import FileUploader, { FileUploaderRef } from "@/components/Ui/FileUploader";
import Input from "@/components/Ui/Input";
import Select from "@/components/Ui/Select";
import Map from "@/components/Ui/Map";
import { useLocationStore } from "@/store";
import { TLocation, TLocationLatLng } from "@/types";
import React, { useEffect, useRef, useState } from "react";

const ShareLocationForm = () => {
  const {
    addLocation,
    locations,
    selectedLocation,
    setSelectedLocation,
    editLocation,
  } = useLocationStore();
  const [currentLocation, setCurrentLocation] = useState<
    TLocationLatLng | undefined
  >();

  const fileUploaderRef = useRef<FileUploaderRef | null>(null);

  const [locationForm, setLocationForm] = useState<TLocation>({
    id: 0,
    locationName: "",
    locationType: "business",
    logo: "",
    lng: 51.348576,
    lat: 35.73689,
  });

  useEffect(() => {
    if (selectedLocation) {
      setLocationForm(selectedLocation);
      setCurrentLocation({
        lat: selectedLocation.lat?.toString() || "",
        lng: selectedLocation.lng?.toString() || "",
      });
    }
  }, [selectedLocation]);

  const myOptions = [{ title: "business" }, { title: "home" }];

  // ** we can use useCallback for function to pass to child to prevent rerender children's

  const handleSelect = (selectedValue: string) => {
    setLocationForm((prevState) => ({
      ...prevState,
      locationType: selectedValue,
    }));
  };

  const handleInputValue = (value: string) => {
    setLocationForm((prevState) => ({
      ...prevState,
      locationName: value,
    }));
  };

  const handleFileSelect = async (file: File | null) => {
    if (file) {
      try {
        const formData = new FormData();
        formData.append("file", file);

        const response = await fetch("/api/upload", {
          method: "POST",
          body: formData,
        });

        const result = await response.json();
        setLocationForm((prevState) => ({
          ...prevState,
          logo: result?.fileUrl,
        }));
      } catch (error) {
        console.error("Error uploading file:", error);
      }
    } else {
      console.log("No file selected");
    }
  };

  const handleLocationChange = (value: TLocationLatLng) => {
    setLocationForm((prevState) => ({
      ...prevState,
      lat: value?.lat,
      lng: value?.lng,
    }));
  };

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (selectedLocation) {
      editLocation(Number(locationForm.id), locationForm);
      if (fileUploaderRef.current) {
        fileUploaderRef.current.clearFileInput();
      }
      setLocationForm((prevState) => ({
        ...prevState,
        locationName: "",
        logo: "",
      }));
      setSelectedLocation(null);
      return;
    }

    addLocation({ ...locationForm, id: Math.random() });
    setLocationForm((prevState) => ({
      ...prevState,
      locationName: "",
      logo: "",
    }));
    if (fileUploaderRef.current) {
      fileUploaderRef.current.clearFileInput();
    }
  };

  const handleCancel = (e: React.MouseEvent): void => {
    e.preventDefault();
    setSelectedLocation(null);
    setLocationForm((prevState) => ({
      ...prevState,
      locationName: "",
      logo: "",
    }));
  };

  return (
    <div className="border h-screen p-4 flex flex-col space-y-6 ">
      <h1 className="my-4">Share location</h1>
      <form
        className="h-screen p-4 flex flex-col space-y-6"
        onSubmit={handleSubmit}
      >
        <Input
          labelName="Location name"
          onInputChange={handleInputValue}
          value={locationForm.locationName}
          isRequired
        />

        <Select
          labelName="Location type"
          options={myOptions}
          onSelect={handleSelect}
          customClassName="my-1"
        />

        <FileUploader
          onFileSelect={handleFileSelect}
          typesAccept=".jpg, .jpeg, .png, .svg"
          ref={fileUploaderRef}
        />

        <Map
          currentLocation={currentLocation}
          locationList={locations}
          changeState={(value: TLocationLatLng) => handleLocationChange(value)}
        />

        <Button
          label={selectedLocation ? "edit location" : "share new location"}
        />

        <Button label="cancel" onClick={handleCancel} />
      </form>
    </div>
  );
};

export default ShareLocationForm;
