"use client";

import React, { useState } from "react";
import ShareLocationForm from "./components/ShareLocationForm";
import Map from "@/components/Ui/Map";
import { useLocationStore } from "@/store";
import { TLocationLatLng } from "@/types";

const Locations = () => {
  const [currentLocation, setCurrentLocation] = useState<
    TLocationLatLng | undefined
  >();

  //** we can use shallow => for better performance and prevent to rerender components
  const { locations, deleteLocation, setSelectedLocation } = useLocationStore();

  return (
    <main className="w-full flex">
      <div className="w-2/4">
        <Map
          currentLocation={currentLocation}
          locationList={locations}
          mode="view"
          popupDeleteFunction={deleteLocation}
          popupEditFunction={setSelectedLocation}
        />
      </div>
      <div className="w-2/4">
        <ShareLocationForm />
      </div>
    </main>
  );
};

export default Locations;
