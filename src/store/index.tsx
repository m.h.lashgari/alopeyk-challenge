import { TLocation } from "@/types";
import { create } from "zustand";

interface sharedLocations {
  locations: Array<TLocation>; // or location[]
  selectedLocation: TLocation | null;
  addLocation: (locationObj: TLocation) => void;
  deleteLocation: (locationId: string | number) => void;
  editLocation: (id: number, newLocation: TLocation) => void;
  setSelectedLocation: (locationObj: TLocation | null) => void;
}

//** we can use Immer js for better obj combine and help us to write cleaner obj select or even persist data

export const useLocationStore = create<sharedLocations>((set) => ({
  locations: [],
  selectedLocation: null,
  setSelectedLocation: (locationObj: TLocation | null) => {
    set(() => ({
      selectedLocation: locationObj,
    }));
  },
  addLocation: (locationObj: TLocation) => {
    set((state) => ({
      locations: [...state.locations, locationObj],
    }));
  },
  deleteLocation: (id: number | string) => {
    set((state) => ({
      locations: state.locations.filter((location) => location.id !== id),
    }));
  },
  editLocation: (id: number, newLocation: TLocation) => {
    set((state) => ({
      locations: state.locations.map((location) =>
        location.id === id ? newLocation : location
      ),
    }));
  },
}));
