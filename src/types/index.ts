export type TLocationLatLng = {
  lat: string;
  lng: string;
};

export type TLocation = {
  id: number | string;
  locationName: string | number | null;
  locationType: string;
  lat?: string | number;
  lng?: string | number;
  logo?: string;
};
