import React, {
  ChangeEvent,
  forwardRef,
  useImperativeHandle,
  useRef,
} from "react";

interface FileUploaderProps {
  onFileSelect: (file: File | null) => void;
  typesAccept: string;
}

export type FileUploaderRef = {
  clearFileInput: () => void;
};

const FileUploader = forwardRef<FileUploaderRef, FileUploaderProps>(
  ({ onFileSelect, typesAccept }, ref) => {
    const fileInputRef = useRef<HTMLInputElement>(null);

    const handleFileChange = (e: ChangeEvent<HTMLInputElement>) => {
      const selectedFile = e.target.files?.[0] || null;
      onFileSelect(selectedFile);
    };

    // Expose a method to clear the file input externally
    useImperativeHandle(ref, () => ({
      clearFileInput: () => {
        if (fileInputRef.current) {
          fileInputRef.current.value = "";
        }
      },
    }));

    return (
      <div className="my-2">
        <label
          htmlFor="fileInput"
          className="block text-sm font-medium text-gray-700"
        >
          Upload File
        </label>
        <input
          type="file"
          id="fileInput"
          className="mt-1 p-2 border rounded-md w-full"
          onChange={handleFileChange}
          accept={typesAccept}
          ref={fileInputRef}
        />
      </div>
    );
  }
);

export default FileUploader;
