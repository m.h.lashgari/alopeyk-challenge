"use client";

import React, { useState } from "react";

interface SelectProps {
  options: Array<{ title: string }>;
  onSelect: (selectedValue: string) => void;
  labelName: string;
  customClassName?: string;
  defaultValue?: string;
}

const Select = ({
  options,
  onSelect,
  labelName,
  customClassName,
  defaultValue,
}: SelectProps) => {
  const [selectedValue, setSelectedValue] = useState<string>("");

  const handleSelectChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const value = e.target.value;
    setSelectedValue(value);
    onSelect(value);
  };

  return (
    <div className={customClassName}>
      <label htmlFor={labelName}>{labelName}</label> :
      <select
        defaultValue={defaultValue}
        id={labelName}
        value={selectedValue}
        onChange={handleSelectChange}
        className="p-2 border rounded mx-5"
      >
        {options.map((option, index) => (
          <option key={index} value={option.title}>
            {option.title}
          </option>
        ))}
      </select>
    </div>
  );
};

export default Select;
