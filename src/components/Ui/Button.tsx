"use client";
import React from "react";

interface ButtonProps {
  label: string;
  onClick?: React.MouseEventHandler<HTMLButtonElement>;
  customClassName?: string;
  type?: "button" | "submit" | "reset" | undefined;
}

const Button = ({ label, onClick, customClassName, type }: ButtonProps) => {
  return (
    <button
      type={type}
      className={`bg-blue-500 text-white px-4 py-2 rounded-md ${customClassName}`}
      onClick={onClick}
    >
      {label}
    </button>
  );
};

export default Button;
