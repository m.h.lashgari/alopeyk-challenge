"use client";

import React, { useRef, useEffect, MutableRefObject } from "react";
import ReactDOM from "react-dom";
import mapboxgl from "mapbox-gl";
import "mapbox-gl/dist/mapbox-gl.css";
import { TLocation, TLocationLatLng } from "@/types";
import Image from "next/image";
import Button from "./Button";

mapboxgl.accessToken = process.env.NEXT_PUBLIC_MAPBOX_ACCESS_TOKEN!;
let apiKey = process.env.NEXT_PUBLIC_MAPBOX_RTL_PLUGIN;

interface MapProps {
  currentLocation?: TLocationLatLng;
  zoom?: number;
  changeState?: (loc: TLocationLatLng) => void;
  locationList?: Array<TLocation>;
  mode?: "add" | "view";
  popupDeleteFunction?: (id: string | number) => void;
  popupEditFunction?: (location: TLocation) => void;
}

const Map = ({
  currentLocation,
  zoom = 12,
  changeState,
  locationList = [],
  mode = "add",
  popupDeleteFunction,
  popupEditFunction,
}: MapProps) => {
  const mapContainer = useRef(null);
  const map = useRef(null) as MutableRefObject<any>;
  const markers = useRef([] as mapboxgl.Marker[]);

  //add rtl plugin
  useEffect(() => {
    if (mapboxgl.getRTLTextPluginStatus() === "unavailable") {
      mapboxgl.setRTLTextPlugin(
        process.env.NEXT_PUBLIC_MAPBOX_RTL_URL!,
        (): void => {},
        true
      );
    }
  }, []);

  // set marker center
  const updateMarkerPosition = () => {
    if (changeState) {
      const center = map.current.getCenter();
      markers.current[0].setLngLat([center.lng, center.lat]).addTo(map.current);
      changeState({
        lng: center.lng.toFixed(4),
        lat: center.lat.toFixed(4),
      });
    }
  };

  //set initial marker and position of map
  useEffect(() => {
    map.current = new mapboxgl.Map({
      container: mapContainer.current!,
      style: `https://map.ir/vector/styles/main/mapir-xyz-style.json?x-api-key=${apiKey}`,
      center:
        currentLocation && currentLocation.lng
          ? [Number(currentLocation.lng), Number(currentLocation.lat)]
          : [51.348576, 35.73689],
      zoom: zoom,
      transformRequest: (url) => {
        return {
          url: url,
          headers: { "x-api-key": apiKey },
        };
      },
    });

    if (mode === "add" && changeState) {
      const el = document.createElement("div");
      el.className = "marker";
      //@ts-ignore
      markers.current[0] = new mapboxgl.Marker({ el });

      map.current.on("load", () => {
        updateMarkerPosition();
      });
      map.current.on("drag", () => {
        updateMarkerPosition();
      });
      map.current.on("zoom", () => {
        updateMarkerPosition();
      });
    }

    return () => {
      if (map.current) {
        map.current.remove();
      }
    };
  }, []);

  // set marker and position in edit mode
  useEffect(() => {
    if (currentLocation && currentLocation.lng && currentLocation.lat) {
      map?.current?.setCenter([
        parseFloat(currentLocation.lng),
        parseFloat(currentLocation.lat),
      ]);
      updateMarkerPosition();
    }
  }, [currentLocation]);

  // show and add markers in view mode and also add popup
  useEffect(() => {
    if (mode === "view") {
      if (locationList.length) {
        markers.current.forEach((marker) => marker.remove());
        markers.current = [];

        for (let i = 0; i < locationList.length; i++) {
          const location = locationList[i];
          const el = document.createElement("div");
          el.className = `marker ${location.id}`;
          el.id = location.id.toString();

          const popupNode = document.createElement("div");

          const deleteButtonHandler = () => {
            if (popupDeleteFunction) {
              popupDeleteFunction(location.id);

              if (markers.current.length === 1) {
                markers.current[0].remove();
              } else {
                const marker = markers.current.find(
                  (marker) => marker.getElement()?.id === location.id.toString()
                );
                if (marker) {
                  marker.remove();
                  markers.current = markers.current.filter((m) => m !== marker);
                }
              }

              el.remove();
            }
          };

          const editButtonHandler = () => {
            if (popupEditFunction) {
              popupEditFunction(location);
            }
          };

          //render popup
          ReactDOM.render(
            <div className="flex flex-col space-y-2 border p-3 rounded-md">
              <h5> location name : {location.locationName}</h5>
              <span>location type: {location.locationType}</span>
              {location.logo ? (
                <Image
                  src={location.logo!}
                  alt={location.locationType}
                  width={200}
                  height={200}
                />
              ) : null}
              <Button label="Delete" onClick={deleteButtonHandler} />
              <Button label="edit" onClick={editButtonHandler} />
            </div>,
            popupNode
          );
          //@ts-ignore
          const marker = new mapboxgl.Marker({ el })
            .setLngLat([Number(location.lng), Number(location.lat)])
            .setPopup(
              new mapboxgl.Popup({ offset: 25 }).setDOMContent(popupNode)
            )
            .addTo(map.current);

          markers.current.push(marker);
        }
        map.current.setCenter([
          locationList[locationList.length - 1].lng,
          locationList[locationList.length - 1].lat,
        ]);
      }
    }
  }, [locationList, mode]);

  return <div ref={mapContainer} className="map-container" />;
};

export default Map;
