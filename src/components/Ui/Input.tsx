import { ChangeEvent, DetailedHTMLProps, InputHTMLAttributes } from "react";

interface InputProps {
  labelName: string;
  onInputChange: (value: string) => void;
  isRequired?: boolean;
  value?: any;
}

const Input = ({
  labelName,
  onInputChange,
  isRequired = false,
  value,
}: InputProps) => {
  const handleInputChange = (e: ChangeEvent<HTMLInputElement>) => {
    const value = e.target.value;
    onInputChange(value);
  };
  return (
    <div>
      <label htmlFor={labelName}>{labelName}</label> :
      <input
        className="mx-2 border-2 rounded-md p-1"
        id={labelName}
        onChange={handleInputChange}
        required={isRequired}
        value={value}
      />
    </div>
  );
};

export default Input;
